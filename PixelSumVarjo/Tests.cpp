//
//  Tests.cpp
//  PixelSumVarjo
//
//  Created by A$AP MAC on 9/16/18.
//  Copyright © 2018 A$AP MAC. All rights reserved.
//
#include <iostream>
#include <ctime>
#include "Tests.h"
#include "PixelSum.h"

using namespace std;

void TestCreation()
{
    cout << "--" << endl;
    PixelSum zero = PixelSum::generateCounterBuffer(0, 0, "Empty");
    assert(zero.toString() == "");
    assert(zero.summedTableToString() == "");
    
    PixelSum verticalZero = PixelSum::generateCounterBuffer(1, 0, "Empty");
    assert(verticalZero.toString() == "");
    assert(verticalZero.summedTableToString() == "");
    
    PixelSum one = PixelSum::generateCounterBuffer(1, 1, "One");
    string onePrint = one.toString();
    assert(onePrint == "0");
    string oneTable = one.summedTableToString();
    assert(oneTable == "0");
    
    PixelSum oneBy3 = PixelSum::generateCounterBuffer(1, 3, "OneByThree");
    string oneBy3Expected = "0\n1\n2";
    string oneBy3Print = oneBy3.toString();
    assert(oneBy3Expected == oneBy3Print);
    
    // summed table
    string oneby3sum = "0\n1\n3";
    string oneBy3sumActual = oneBy3.summedTableToString();
    assert(oneby3sum == oneBy3sumActual);
    
    PixelSum threeBy1 = PixelSum::generateCounterBuffer(3, 1, "ThreeByOne");
    string threeBy1Expected = "0,1,2";
    string threeBy1Print = threeBy1.toString();
    assert(threeBy1Expected == threeBy1Print);
    
    // table area sum
    string threeby1sumExpected = "0,1,3";
    string threeby1sumPrint = threeBy1.summedTableToString();
    assert(threeby1sumExpected == threeby1sumPrint);
    
    PixelSum twoBy4 = PixelSum::generateCounterBuffer(2, 4, "Two by four");
    string twoBy4Expected = "0,1\n2,3\n4,5\n6,7";
    string twoBy4Print = twoBy4.toString();
    assert(twoBy4Expected == twoBy4Print);
    
    // summed area table
    string twoby4sum = "0,1\n2,6\n6,15\n12,28";
    string twoby4sumPrint = twoBy4.summedTableToString();
    assert(twoby4sum == twoby4sumPrint);
    
    // check value constructor
    PixelSum fourOnes(1,2,2);
    string fourOnesExpected = "1,1\n1,1";
    string fourOnesPrint = fourOnes.toString();
    assert(fourOnesExpected == fourOnesPrint);
    
    string fourOnesSum = "1,2\n2,4";
    string fourOnesSumPrint = fourOnes.summedTableToString();
    assert(fourOnesSum == fourOnesSumPrint);
    
    // check assignment operator
    PixelSum copyCheck(one);
    copyCheck.Name = "Copy";
    string copyCheckPrint = copyCheck.toString();
    assert(copyCheckPrint == "0");
    
    copyCheck = twoBy4;
    copyCheckPrint = copyCheck.toString();
    assert(twoBy4Expected == copyCheckPrint);
    
    cout << "Creation tests passed succesfully" << endl;
}

void GetPixelSumTest()
{
    PixelSum twoByTwo(1,2,2);
    double twoby2Sum = twoByTwo.getPixelSum(0, 0, 1, 1);
    assert(twoby2Sum == 4);
    
    double singlePixelSum = twoByTwo.getPixelSum(0, 0, 0, 0);
    assert(singlePixelSum == 1);
    
    double twoby2OnePixelSum = twoByTwo.getPixelSum(1, 1, 3, 3);
    assert(twoby2OnePixelSum == 1);
    
    double outsideOfBufferSum = twoByTwo.getPixelSum(2, 2, 50, 50);
    assert(outsideOfBufferSum == 0);
    
    PixelSum threeByFiveMatrix = PixelSum::generateCounterBuffer(3, 5, "3x5");
    double sum = threeByFiveMatrix.getPixelSum(0, 0, 1, 1);
    assert(sum == 8);
    
    double twoByThreeSum = threeByFiveMatrix.getPixelSum(0, 0, 1, 2);
    assert(twoByThreeSum == 21);
    
    double fullMatrixSum = threeByFiveMatrix.getPixelSum(0, 0, 2, 4);
    assert(fullMatrixSum == 105);
    
    double bottomPartSum = threeByFiveMatrix.getPixelSum(0, 4, 2, 4);
    assert(bottomPartSum == 39);
    
    double topPartSum = threeByFiveMatrix.getPixelSum(0, 0, 2, 0);
    assert(topPartSum == 3);
    
    double swappedBoundsSum = threeByFiveMatrix.getPixelSum(2, 0, 0, 0);
    assert(swappedBoundsSum == 3);
    
    double centerSum = threeByFiveMatrix.getPixelSum(1, 1, 2, 2);
    assert(centerSum == 24);
    
    cout << "Sum methods tests passed successfully" << endl;
}

void GetPixelAverageTest()
{
    PixelSum twoByTwo(1,2,2);
    double twoby2avrg = twoByTwo.getPixelAverage(0, 0, 1, 1);
    assert(twoby2avrg == 1);
    
    double singlePixelAvrg = twoByTwo.getPixelAverage(0, 0, 0, 0);
    assert(singlePixelAvrg == 1);
    
    double average1by9 = twoByTwo.getPixelAverage(1, 1, 3, 3);
    assert(average1by9 == 1/9.0);
    
    double average4by16 = twoByTwo.getPixelAverage(0, 0, 3, 3);
    assert(average4by16 == 4/16.0);
    
    double outsideOfBufferAverage = twoByTwo.getPixelAverage(2, 2, 50, 50);
    assert(outsideOfBufferAverage == 0);
    
    PixelSum threeByFiveMatrix = PixelSum::generateCounterBuffer(3, 5, "3x5");
    double average = threeByFiveMatrix.getPixelAverage(0, 0, 1, 1);
    assert(average == 2);
    
    double twoByThreeAverage = threeByFiveMatrix.getPixelAverage(0, 0, 1, 2);
    assert(twoByThreeAverage == 3.5);
    
    double threeby5Avrg = threeByFiveMatrix.getPixelAverage(0, 0, 2, 4);
    assert(threeby5Avrg == 7);
    
    double hundredElemsAvrg = threeByFiveMatrix.getPixelAverage(0, 0, 9, 9);
    assert(hundredElemsAvrg == 1.05);
    
    double bottomPartAverage = threeByFiveMatrix.getPixelAverage(0, 4, 2, 4);
    assert(bottomPartAverage == 13);
    
    double topPartAverage = threeByFiveMatrix.getPixelAverage(0, 0, 2, 0);
    assert(topPartAverage == 1);
    
    double swappedBoundsAverage = threeByFiveMatrix.getPixelAverage(2, 0, 0, 0);
    assert(swappedBoundsAverage == 1);
    
    double centerAverage = threeByFiveMatrix.getPixelAverage(1, 1, 2, 2);
    assert(centerAverage == 6);
    
    cout << "Average methods tests passed successfully" << endl;
}

void GetNonZeroSumsTest()
{
    PixelSum zeroBuffer((size_t)0, 1, 1);
    assert(zeroBuffer.getNonZeroCount(0, 0, 0, 0) == 0);
    
    PixelSum twobyTwo((size_t)1, 2, 2);
    assert(twobyTwo.getNonZeroCount(0, 0, 1, 1) == 4);
    
    PixelSum threeByFiveSwitch = PixelSum::generateSwitchBuffer(3, 5, "3x5");
    //    cout << threeByFiveSwitch.toString() << endl;
    // first row
    assert(threeByFiveSwitch.getNonZeroCount(0, 0, 2, 0) == 1);
    // second row
    assert(threeByFiveSwitch.getNonZeroCount(0, 1, 2, 1) == 2);
    
    // first column
    assert(threeByFiveSwitch.getNonZeroCount(0, 0, 0, 4) == 2);
    // second column
    assert(threeByFiveSwitch.getNonZeroCount(1, 0, 1, 4) == 3);
    
    // center range
    assert(threeByFiveSwitch.getNonZeroCount(1, 1, 2, 2) == 2);
    
    // the one from email  [0, 4, 0, 2, 1, 0]
    BufferType* emailBuffer = new BufferType[3*2];
    // first row
    emailBuffer[0] = 0;
    emailBuffer[1] = 4;
    emailBuffer[2] = 0;
    // second row
    emailBuffer[3] = 2;
    emailBuffer[4] = 1;
    emailBuffer[5] = 0;
    
    PixelSum emailMatrix(emailBuffer, 3, 2);
    //    cout << emailMatrix.toString() << endl;
    assert(emailMatrix.getNonZeroCount(0, 0, 2, 1) == 3);
    assert(emailMatrix.getNonZeroAverage(0, 0, 2, 1) == 7/3);
    // outside of the buffer
    assert(emailMatrix.getNonZeroAverage(3, 3, 3, 3) == 0);
    
    cout << "Non zero sum test passed succesfully" << endl;
}

void PerformanceTest()
{
    // MacBook Pro 2015, 13''
    
    // 512 buffer speed test ~4ms
    {
        clock_t start = clock();
        
        PixelSum twoKBuffer(1, 512, 512);
        clock_t finish = clock();
        
        double elapsedTime = 1.0 * (finish - start) / CLOCKS_PER_SEC * 1000;
        cout << "512x512 build time " << elapsedTime << "ms" << endl;
        
        start = clock();
        twoKBuffer.getPixelAverage(0, 0, 511, 511);
        finish = clock();
        
        elapsedTime = 1.0 * (finish - start) / CLOCKS_PER_SEC * 1000;
        cout << "512x512 average count took " << elapsedTime << "ms. "<< endl;
    }
    
    // 1k buffer speed test ~16ms
    {
        clock_t start = clock();
        
        PixelSum twoKBuffer(1, 1024, 1024);
        clock_t finish = clock();
        
        double elapsedTime = 1.0 * (finish - start) / CLOCKS_PER_SEC * 1000;
        cout << "1024x1024 build time " << elapsedTime << "ms" << endl;
        
        start = clock();
        twoKBuffer.getPixelAverage(0, 0, 1023, 1023);
        finish = clock();
        
        elapsedTime = 1.0 * (finish - start) / CLOCKS_PER_SEC * 1000;
        cout << "1024x1024 average count took " << elapsedTime << "ms. "<< endl;
    }
    
    // 2k buffer speed test ~60ms
    {
        clock_t start = clock();
        
        PixelSum twoKBuffer(1, 2048, 2048);
        clock_t finish = clock();
        
        double elapsedTime = 1.0 * (finish - start) / CLOCKS_PER_SEC * 1000;
        cout << "2048x2048 build time " << elapsedTime << "ms" << endl;
        
        start = clock();
        twoKBuffer.getPixelAverage(0, 0, 2047, 2047);
        finish = clock();
        
        elapsedTime = 1.0 * (finish - start) / CLOCKS_PER_SEC * 1000;
        cout << "2048x2048 average count took " << elapsedTime << "ms. "<< endl;
    }
    
    // 4k buffer speed test ~350ms
    {
        clock_t start = clock();
        
        PixelSum fourKbuffer(1, 4096, 4096);
        clock_t finish = clock();
        
        double elapsedTime = 1.0 * (finish - start) / CLOCKS_PER_SEC * 1000;
        cout << "4096x4096 build time " << elapsedTime << "ms" << endl;
        
        start = clock();
        fourKbuffer.getPixelAverage(0, 0, 4095, 4095);
        finish = clock();
        
        elapsedTime = 1.0 * (finish - start) / CLOCKS_PER_SEC * 1000;
        cout << "4096x4096 average count took " << elapsedTime << "ms. "<< endl;
    }
    
    // 8k buffer speed test ~1300 ms
    {
        clock_t start = clock();
        PixelSum eightKbuffer(1, 8192, 8192);
        clock_t finish = clock();
        
        double elapsedTime = 1.0 * (finish - start) / CLOCKS_PER_SEC * 1000;
        cout << "8192x8192 build time " << elapsedTime << "ms" << endl;
        
        start = clock();
        eightKbuffer.getPixelAverage(0, 0, 8191, 8191);
        finish = clock();
        
        elapsedTime = 1.0 * (finish - start) / CLOCKS_PER_SEC * 1000;
        cout << "8192x8192 average count took " << elapsedTime << "ms. "<< endl;
    }
    
    // 16k buffer speed test ~ [4sec, 8sec]
    {
        clock_t start = clock();
        PixelSum sixteenKbuffer(1, 16384, 16384);
        clock_t finish = clock();
        
        double elapsedTime = 1.0 * (finish - start) / CLOCKS_PER_SEC * 1000;
        cout << "16384x16384 build time " << elapsedTime << "ms" << endl;
        
        start = clock();
        sixteenKbuffer.getPixelAverage(0, 0, 8191, 8191);
        finish = clock();
        
        elapsedTime = 1.0 * (finish - start) / CLOCKS_PER_SEC * 1000;
        cout << "16384x16384 average count took " << elapsedTime << "ms. "<< endl;
    }
}
