//
//  PixelSum.h
//  ProductSumVarjo
//
//  Created by A$AP MAC on 9/4/18.
//  Copyright © 2018 A$AP MAC. All rights reserved.
//

#ifndef PixelSum_h
#define PixelSum_h

#include <iostream>
using namespace std;

typedef unsigned char BufferType;

class PixelSum
{
public:
    PixelSum(const BufferType* buffer, size_t width, size_t height, string name = "");
    // copies the value to each buffer slot
    PixelSum(BufferType value, size_t width, size_t height, string name = "");
    PixelSum(const PixelSum& other);
    PixelSum& operator=(const PixelSum&);
    ~PixelSum();
    
    BufferType getPixelSum(size_t x0, size_t y0, size_t x1, size_t y1) const;
    double getPixelAverage(size_t x0, size_t y0, size_t x1, size_t y1) const;
    
    size_t getNonZeroCount(size_t x0, size_t y0, size_t x1, size_t y1) const;
    double getNonZeroAverage(size_t x0, size_t y0, size_t x1, size_t y1) const;
    
    string toString() const;
    string summedTableToString() const;
    
    // 0, 1, 2, 3...
    static PixelSum generateCounterBuffer(size_t width, size_t height, string name);
    // 0, 1, 0, 1...
    static PixelSum generateSwitchBuffer(size_t width, size_t height, string name);
private:
    void buildSummedAreaTable();
    size_t getIndex(size_t x0, size_t y0) const;
    
public:
    string Name;
private:
    BufferType* buffer;
    unsigned int* summedAreaTable; // https://en.wikipedia.org/wiki/Summed-area_table
    unsigned short* nonZeroTable; // 
    
    size_t width;
    size_t height;
    size_t id;

    static size_t sId;
    PixelSum();
};

#endif /* PixelSum_h */
