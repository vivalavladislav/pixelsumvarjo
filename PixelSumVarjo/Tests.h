//
//  Tests.hpp
//  PixelSumVarjo
//
//  Created by A$AP MAC on 9/16/18.
//  Copyright © 2018 A$AP MAC. All rights reserved.
//

#ifndef Tests_hpp
#define Tests_hpp

void TestCreation();
void GetPixelSumTest();
void GetPixelAverageTest();
void GetNonZeroSumsTest();
void PerformanceTest();

#endif /* Tests_hpp */
