//
//  PixelSum.cpp
//  ProductSumVarjo
//
//  Created by A$AP MAC on 9/4/18.
//  Copyright © 2018 A$AP MAC. All rights reserved.
//

#include <stdio.h>
#include "PixelSum.h"
#include <iostream>

using namespace std;

size_t PixelSum::sId = 0;

// deals with ranges
struct Window
{
    Window(size_t x0,size_t y0,size_t x1,size_t y1)
    :x0(x0),y0(y0),x1(x1),y1(y1)
    {
    }
    
    size_t Size() const
    {
        return (x1-x0 +1) * (y1 - y0 +1);
    };
    
    size_t x0;
    size_t y0;
    size_t x1;
    size_t y1;
};

// 0, 1, 2, 3...
PixelSum PixelSum::generateCounterBuffer(size_t width, size_t height, string name)
{
    BufferType* buffer = new BufferType[width * height];
    size_t counter = 0;
    
    // filling up the buffer column by column
    for(size_t column = 0; column < height; ++column)
    {
        for(size_t row = 0; row < width; ++row)
        {
            size_t index = row + column * width;
            buffer[index] = counter++;
        }
    }
    
    PixelSum result(buffer, width, height, name);
    
    delete[] buffer;
    buffer = 0;
    
    return result;
}

// 0, 1, 0, 1...
PixelSum PixelSum::generateSwitchBuffer(size_t width, size_t height, string name)
{
    BufferType* buffer = new BufferType[width * height];
    
    // filling up the buffer column by column
    for(size_t column = 0; column < height; ++column)
    {
        for(size_t row = 0; row < width; ++row)
        {
            size_t index = row + column * width;
            buffer[index] = index % 2;
        }
    }
    
    PixelSum result(buffer, width, height, name);
    
    delete[] buffer;
    buffer = 0;
    
    return result;
}

PixelSum::PixelSum(const BufferType* buffer, size_t width, size_t height, string name)
    :id(sId++), width(width), height(height), buffer(0), summedAreaTable(0), Name(name), nonZeroTable(0)
{
    this->buffer = new BufferType[width * height];
    summedAreaTable = new unsigned int[width * height];
    nonZeroTable = new unsigned short[width * height];
    
    for(size_t column = 0; column < height; ++column)
    {
        for(size_t row = 0; row < width; ++row)
        {
            size_t index = row + column * width;
            this->buffer[index] = buffer[index];
        }
    }
    buildSummedAreaTable();
}

PixelSum::PixelSum(BufferType value, size_t width, size_t height, string name)
    :id(sId++), width(width), height(height), buffer(0), summedAreaTable(0), Name(name), nonZeroTable(0)
{
    this->buffer = new BufferType[width * height];
    summedAreaTable = new unsigned int[width * height];
    nonZeroTable = new unsigned short[width * height];
    
    for(size_t column = 0; column < height; ++column)
    {
        for(size_t row = 0; row < width; ++row)
        {
            size_t index = row + column * width;
            this->buffer[index] = value;
        }
    }
    
    buildSummedAreaTable();
}

PixelSum::PixelSum(const PixelSum& other)
    :id(sId++), width(other.width), height(other.height), buffer(0), Name(other.Name), summedAreaTable(0), nonZeroTable(0)
{
    buffer = new BufferType[width * height];
    summedAreaTable = new unsigned int[width * height];
    nonZeroTable = new unsigned short[width * height];
    
    for(size_t column = 0; column < height; ++column)
    {
        for(size_t row = 0; row < width; ++row)
        {
            size_t index = row + column * width;
            
            buffer[index] = other.buffer[index];
            summedAreaTable[index] = other.summedAreaTable[index];
            nonZeroTable[index] = other.nonZeroTable[index];
        }
    }
}

PixelSum& PixelSum::operator=(const PixelSum& other)
{
    delete[] buffer;
    delete[] summedAreaTable;
    
    width = other.width;
    height = other.height;
    
    buffer = new BufferType[width * height];
    summedAreaTable = new unsigned int[width * height];
    nonZeroTable = new unsigned short[width * height];
    
    for (int column = 0; column < height; ++column)
    {
        for (int row = 0; row < width; ++row)
        {
            size_t index = row + column * width;
            
            buffer[index] = other.buffer[index];
            summedAreaTable[index] = other.summedAreaTable[index];
            nonZeroTable[index] = other.nonZeroTable[index];
        }
    }
    
    return *this;
}

PixelSum::~PixelSum()
{
    delete[] buffer;
    buffer = 0;
    
    delete[] summedAreaTable;
    summedAreaTable = 0;
    
    delete [] nonZeroTable;
    nonZeroTable = 0;
}

void PixelSum::buildSummedAreaTable()
{
    if(width <= 0 || height <= 0)
        return;
    
    summedAreaTable[0] = buffer[0];
    nonZeroTable[0] = buffer[0] != 0;
    
    // build first row of summed area table
    for(int row = 1; row < width; ++ row)
    {
        summedAreaTable[row] = buffer[row] + summedAreaTable[row-1];
        nonZeroTable[row] = (buffer[row] != 0) + nonZeroTable[row-1];
    }
    
    size_t currentIndex = 0;
    size_t prevColumn = 0;
    
    // build first column
    for(int column = 1; column < height; ++column)
    {
        currentIndex = column * width;
        prevColumn = (column - 1) * width;
        
        summedAreaTable[currentIndex] = buffer[currentIndex] + summedAreaTable[prevColumn];
        nonZeroTable[currentIndex] = (buffer[currentIndex] != 0) + nonZeroTable[prevColumn];
    }
    
    
    // build the rest of the table
    for(int column = 1; column < height; ++column)
    {
        for (int row = 1; row < width; ++row)
        {
            currentIndex = row + column * width;
            prevColumn = currentIndex - width;
            
            // this element + previous horizontal sum + previous vertical sum - (intersection of prev horizontal and vertical sum)
            summedAreaTable[currentIndex] = buffer[currentIndex] + summedAreaTable[currentIndex - 1] + summedAreaTable[prevColumn] - summedAreaTable[prevColumn -1];
            nonZeroTable[currentIndex] = (buffer[currentIndex] != 0) + nonZeroTable[currentIndex-1] + nonZeroTable[prevColumn] - nonZeroTable[prevColumn -1];
        }
    }
}

BufferType PixelSum::getPixelSum(size_t x0, size_t y0, size_t x1, size_t y1) const
{
    // the window with correct bounds
    Window bounds(min(x0, x1), min(y0, y1), max(x0, x1), max(y0, y1));
    
    // https://en.wikipedia.org/wiki/Summed-area_table
    // sum(range) = sum(D) + sum(A) - sum(B) - sum(C)
    // bottom right corner = the sum of the whole range up to that point
    BufferType sum = 0;
    // calculation only can take place if the minimum bounds are respected
    if(bounds.x0 < width && bounds.y0 < height)
    {
        bounds.x1 = min(bounds.x1, width - 1);
        bounds.y1 = min(bounds.y1, height - 1);
        
        sum += summedAreaTable[getIndex(bounds.x1, bounds.y1)];
        
        // top left corner
        int prevX = bounds.x0 - 1;
        int prevY = bounds.y0 - 1;
        if(prevX >= 0 && prevY >= 0)
            sum += summedAreaTable[getIndex(prevX, prevY)];
        
        // bottom left corner
        if(prevX >= 0)
            sum -= summedAreaTable[getIndex(prevX, bounds.y1)];
        
        // top right corner
        if(prevY >= 0)
            sum -= summedAreaTable[getIndex(bounds.x1, prevY)];
    }
    
    return sum;
}

double PixelSum::getPixelAverage(size_t x0, size_t y0, size_t x1, size_t y1) const
{
    Window bounds(min(x0, x1), min(y0, y1), max(x0, x1), max(y0, y1));
    return 1.0 * getPixelSum(x0, y0, x1, y1) / bounds.Size();
}

size_t PixelSum::getNonZeroCount(size_t x0, size_t y0, size_t x1, size_t y1) const
{
    // the window with correct bounds
    Window bounds(min(x0, x1), min(y0, y1), max(x0, x1), max(y0, y1));
    
    // same implementation as with sumAreaTable
    size_t nonZeroCount = 0;
    // calculation only can take place if the minimum bounds are respected
    if(bounds.x0 < width && bounds.y0 < height)
    {
        bounds.x1 = min(bounds.x1, width - 1);
        bounds.y1 = min(bounds.y1, height - 1);
        
        nonZeroCount += nonZeroTable[getIndex(bounds.x1, bounds.y1)];
        
        // top left corner
        int prevX = bounds.x0 - 1;
        int prevY = bounds.y0 - 1;
        if(prevX >= 0 && prevY >= 0)
            nonZeroCount += nonZeroTable[getIndex(prevX, prevY)];
        
        // bottom left corner
        if(prevX >= 0)
            nonZeroCount -= nonZeroTable[getIndex(prevX, bounds.y1)];
        
        // top right corner
        if(prevY >= 0)
            nonZeroCount -= nonZeroTable[getIndex(bounds.x1, prevY)];
    }
    
    return nonZeroCount;
}

double PixelSum::getNonZeroAverage(size_t x0, size_t y0, size_t x1, size_t y1) const
{
    size_t nonZeroCount = getNonZeroCount(x0, y0, x1, y1);
    if(nonZeroCount == 0)
        return 0;
    return getPixelSum(x0, y0, x1, y1) / nonZeroCount;
}

size_t PixelSum::getIndex(size_t x0, size_t y0) const
{
    return x0 + y0 * width;
}

string PixelSum::toString() const
{
    string log = "";
    
    for(int column = 0; column < height; ++column)
    {
        for(int row = 0; row < width; ++row)
        {
            log += to_string(buffer[getIndex(row, column)]);
            if(row != width - 1)
                log += ',';
        }
        
        // omit extra line end
        if(column != height -1)
            log += '\n';
    }
    
    return log;
}

string PixelSum::summedTableToString() const
{
    string log = "";
    
    for(int column = 0; column < height; ++column)
    {
        for(int row = 0; row < width; ++row)
        {
            log += to_string(summedAreaTable[getIndex(row, column)]);
            if(row != width - 1)
                log += ',';
        }
        
        // omit extra line end
        if(column != height -1)
            log += '\n';
    }
    
    return log;
}

