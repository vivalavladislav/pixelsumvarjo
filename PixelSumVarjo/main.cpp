//
//  main.cpp
//  ProductSumVarjo
//
//  Created by A$AP MAC on 9/4/18.
//  Copyright © 2018 A$AP MAC. All rights reserved.
//

#include <iostream>
#include "Tests.h"

using namespace std;

int main(int argc, const char * argv[])
{
    cout << "Vlad Sviatetskyi for Varjo. 17.09.2018." << endl;
    cout << "Pixel Sum" << endl;
    cout << "Implementation of constant-time access to sum of range in two-dimensional buffer." << endl;
    cout << "Utilizes summed area table as a solution for constant time access." << endl;

    TestCreation();
    
    GetPixelSumTest();
    GetPixelAverageTest();
    GetNonZeroSumsTest();
    
    cout << "--" << endl;
    cout << "Performance tests." << endl;
    PerformanceTest();
    
    cout << "--" << endl;
    cout << "Tests passed succesfully. End of the program." << endl;
    return 0;
}
